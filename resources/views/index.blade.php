@extends('layout')

@section('content')
    <div class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-md-8">

                    @if (!empty($comments))
                        <div class="bottom-comment"><!--bottom comment-->
                            <h4>{{count($comments)}} comments</h4>

                                <div class="comment-text">
                                    {{--<a href="#" class="replay btn pull-right"> Replay</a>--}}
                                    <h5>{{$comment->author->name}}</h5>
                                    <p class="comment-date">
                                        {{$comment->created_at->diffForHumans()}}
                                    </p>


                                    <p class="para">{{$comment->text}}</p>
                                </div>
                        </div>
                    @endif
                    <!-- end bottom comment-->

                        <div class="leave-comment"><!--leave comment-->
                            @if(session('status'))
                                <div class="alert alert-success">{{session('status')}}</div>
                                <br>
                            @endif
                            <h4>Leave a reply</h4>

                            <form class="form-horizontal contact-form" role="form" method="post" action="/comment">
                                {{csrf_field()}}
                                <input type="hidden" name="post_id" value="">
                                <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">                                    <div class="col-md-12">
                                <div class="form-group">
                                            <textarea class="form-control" rows="6" name="text"
                                                      placeholder="Write Massage"></textarea>
                                    </div>
                                </div>
                                <button type="submit" class="btn send-btn">Post Comment</button>
                            </form>
                        </div><!--end leave comment-->
                </div>
            </div>
        </div>
    </div>
    @endsection
