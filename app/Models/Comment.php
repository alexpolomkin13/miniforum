<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'text',
    ];

    public static function add($fields)
    {
        $comment = new static;
        $comment->fill($fields);
        $comment->save();

        return $comment->id;
    }

}
